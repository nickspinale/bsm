import Data.List

row0 = repeat 1

step row' = let row = zipWith (+) (tail row') (0:row) in row

rows = iterate step row0

paths = head (rows !! 7)

-- prepend zero squares to rows
board = zipWith (++) (inits (repeat 0)) rows

-- print top-left n x n of infinite board
display n = mapM_ putStrLn . take n $ map (intercalate "\t" . map show . take n) board

main = display 8
