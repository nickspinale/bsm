main = print (go 0)
  where
    go c = if mod c 99 == 12 && mod c 101 == 22 then c else go (c + 1)
