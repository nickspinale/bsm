import Control.Monad

main = go 0
  where
    go 9999 = return ()
    go c = do
        when (mod c 99 == 12 && mod c 101 == 22) (print c)
        go (c + 1)
