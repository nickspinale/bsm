def factor(sigma, pi):
    lo = 0
    hi = sigma/2
    while True:
        mid = (lo + hi)/2
        guess = mid*(sigma - mid)
        if guess < pi:
            lo = mid
        elif guess > pi:
            hi = mid
        else:
            return mid
