import Data.Char

main = getContents >>= mapM_ putStrLn . shifts . filter isUpper
  where
    shift n c = chr (ord 'A' + mod (ord c - ord 'A' + n) 26)
    shifts str = map (flip map str . shift) [0..25]
