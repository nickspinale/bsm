main = mapM_ (putStrLn . format) (step 229513 118577)

step a b = if b == 0
           then []
           else let (q, r) = quotRem a b
                in (a, q, b, r) : step b r

format (a, q, b, r) =
    show a ++ " &= " ++ show q ++ "*" ++ show b ++ " + " ++ show r
